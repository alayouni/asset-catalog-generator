package com.alayouni.assetcatalog;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 */
public enum Device {
	IPHONE_5,
	IPHONE_6,
	IPHONE_6_PLUS,
	IPAD,
	IPAD_RETINA;

	public String deviceSuffix() {
		switch (this) {
			case IPHONE_5:
				return "-R4";
			case IPHONE_6:
				return "@x2";
			case IPHONE_6_PLUS:
				return "@x3";
			case IPAD:
				return "-ipad@x1";
			case IPAD_RETINA:
				return "-ipad@x2";
		}
		return null;
	}
}
