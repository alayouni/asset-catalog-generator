package com.alayouni.assetcatalog;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 */
public class ImageSetGenerator {

	private final Logger logger;

	private final JsonWriter jsonWriter;

	/**
	 * maps each scale to its device suffix so to avoid resizing an image twice when the scale for 2 devices is the same
	 */
	private Map<Double, String> scalesMap = new HashMap<Double, String>();

	private BufferedImage img;

	public ImageSetGenerator(Logger logger) {
		this.logger = logger;
		this.jsonWriter = new JsonWriter(logger);
	}

	public void generateImageSets(double iphone6PlusScale,
	                              double ipadRetinaScale,
	                              double iphone6Scale,
	                              double iphone5Scale,
	                              double ipadScale,
	                              OutputDescriptor[] outputDescriptors) {

		File currentDirectory = new File(".");
		for(File child : currentDirectory.listFiles()) {
			if(child.isFile() && child.getName().endsWith(".png")) {
				try {
					String imageName = child.getName();
					imageName = imageName.substring(0, imageName.length() - ".png".length());
					img = ImageIO.read(child);
					this.generateImageSetsForInput(imageName, iphone6PlusScale, ipadRetinaScale, iphone6Scale, iphone5Scale, ipadScale, outputDescriptors);
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}

		logger.log("Done!\n\n____________________________________________\n\n");
	}

	public void generateImageSetsForInput(String inputImageName,
	                                      double iphone6PlusScale,
	                                      double ipadRetinaScale,
	                                      double iphone6Scale,
	                                      double iphone5Scale,
	                                      double ipadScale,
	                                      OutputDescriptor[] outputDescriptors) {
		scalesMap.clear();

		logger.log("Processing " + inputImageName + ".png");

		initImageSets(inputImageName, outputDescriptors, iphone6PlusScale);
		createImageSetEntriesForDevice(inputImageName, outputDescriptors, ipadRetinaScale, Device.IPAD_RETINA);
		createImageSetEntriesForDevice(inputImageName, outputDescriptors, iphone6Scale, Device.IPHONE_6);
		createImageSetEntriesForDevice(inputImageName, outputDescriptors, iphone5Scale, Device.IPHONE_5);
		createImageSetEntriesForDevice(inputImageName, outputDescriptors, ipadScale, Device.IPAD);

		//persist json files
		for(OutputDescriptor outputDescriptor : outputDescriptors) {
			jsonWriter.persistJson(outputDescriptor.jsonOutputPath(inputImageName), outputDescriptor.getJsonArray());
		}

		logger.log("\n____________________________\n\n");
	}

	private void initImageSets(String inputImageName, OutputDescriptor[] outputDescriptors, double iPhone6PlusScale) {
		try {
			String outputPath;

			//create image set directory
			File outputImageFile, inputImageFile;
			scalesMap.put(iPhone6PlusScale, Device.IPHONE_6_PLUS.deviceSuffix());

			int i = 0;
			if(iPhone6PlusScale != 1.) {
				outputPath = initImageSetForDescriptor(outputDescriptors[0], inputImageName);
				inputImageFile = resizeImage(inputImageName + ".png", outputPath, iPhone6PlusScale, iPhone6PlusScale);
				i = 1;
			} else {
				inputImageFile = new File(inputImageName + ".png");
			}


			for(;i < outputDescriptors.length; i++) {
				outputPath = initImageSetForDescriptor(outputDescriptors[i], inputImageName);
				outputImageFile = new File(outputPath);

				logger.log("copying " + inputImageFile.getName() + " to " + outputPath);
				FileUtils.copyFile(inputImageFile, outputImageFile);
			}
		} catch (IOException e) {
			logger.log(ExceptionUtils.getFullStackTrace(e));
		}
		logger.log("");//line return
	}

	private String initImageSetForDescriptor(OutputDescriptor outputDescriptor, String inputImageName) {
		outputDescriptor.resetJsonArray();
		String imageSetName = outputDescriptor.getPrefix() + inputImageName;
		File imageSetDirectory = new File(outputDescriptor.getParentDirectory() + "/" +imageSetName + ".imageset");

		if(!imageSetDirectory.exists()) {
			imageSetDirectory.mkdir();
		}
		String outputName = imageSetName + Device.IPHONE_6_PLUS.deviceSuffix() + ".png",
		outputPath = outputDescriptor.getParentDirectory() + "/" + imageSetDirectory.getName()  + "/" + outputName;
		jsonWriter.addIPhoneX1EmptyEntry(outputDescriptor.getJsonArray());
		jsonWriter.mapDevice(Device.IPHONE_6_PLUS, outputName, outputDescriptor.getJsonArray());

		return outputPath;
	}

	private void createImageSetEntriesForDevice(String inputImageName, OutputDescriptor[] outputDescriptors, double deviceScale, Device device) {
		if(deviceScale != 0) {
			String actualOutputSuffix;

			if (scalesMap.containsKey(deviceScale)) {
				actualOutputSuffix = scalesMap.get(deviceScale);
			} else {
				actualOutputSuffix = device.deviceSuffix();
				scalesMap.put(deviceScale, actualOutputSuffix);

				String imageSetName = outputDescriptors[0].getPrefix() + inputImageName,
						firstOutputPath = outputDescriptors[0].getParentDirectory() + "/" + imageSetName + ".imageset/" + imageSetName + device.deviceSuffix() + ".png",
						outputPath;
				File resizedImageFile = resizeImage(inputImageName + ".png", firstOutputPath, deviceScale, deviceScale),
						outputImageFile;

				try {
					for (int i = 1; i < outputDescriptors.length; i++) {
						imageSetName = outputDescriptors[i].getPrefix() + inputImageName;
						outputPath = outputDescriptors[i].getParentDirectory() + "/" + imageSetName + ".imageset/" + imageSetName + device.deviceSuffix() + ".png";
						outputImageFile = new File(outputPath);
						logger.log("copying " + resizedImageFile.getPath() + " to " + outputPath);
						FileUtils.copyFile(resizedImageFile, outputImageFile);
					}
				} catch (IOException e) {
					logger.log(ExceptionUtils.getFullStackTrace(e));
				}
			}

			String outputName;
			for (OutputDescriptor outputDescriptor : outputDescriptors) {
				outputName = outputDescriptor.getPrefix() + inputImageName + actualOutputSuffix + ".png";
				jsonWriter.mapDevice(device, outputName, outputDescriptor.getJsonArray());
			}

			logger.log("");//line return
		}
	}

	private File resizeImage(String inputPath, String outputPath, double xScale, double yScale) {
		try {
			double scaledWidth = ((double)img.getWidth()) * xScale,
					scaledHeight = ((double)img.getHeight()) * yScale;

			int newWidth = (int)Math.round(scaledWidth);
			int newHeight = (int)Math.round(scaledHeight);
			File output = new File(outputPath);

			logger.log("resizing " + inputPath + " to " + output.getName() + " to new size " + newWidth + " x " + newHeight + " under " + outputPath);

			Image toolkitImage = img.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);


			// width and height are of the toolkit image
			BufferedImage newImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
			Graphics g = newImage.getGraphics();
			g.drawImage(toolkitImage, 0, 0, null);
			g.dispose();

			ImageIO.write(newImage, "png", output);

			return output;
		} catch (IOException e) {
			logger.log(ExceptionUtils.getFullStackTrace(e));
		}
		return null;
	}


}
