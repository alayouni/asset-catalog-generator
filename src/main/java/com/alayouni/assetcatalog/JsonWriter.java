package com.alayouni.assetcatalog;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 */
public class JsonWriter {
	private final Gson gson;

	private final Logger logger;

	public JsonWriter(Logger logger) {
		gson = new GsonBuilder().setPrettyPrinting().create();
		this.logger = logger;
	}

	public void mapDevice(Device device, String imageName, JsonArray jsonArray) {
		logger.log("mapping " + device + " to " + imageName);
		switch (device) {
			case IPHONE_5:
				addDeviceJsonObject("iphone", "2x", imageName, "retina4", jsonArray);
				break;
			case IPHONE_6:
				addDeviceJsonObject("iphone", "2x", imageName, null, jsonArray);
				break;
			case IPHONE_6_PLUS:
				addDeviceJsonObject("iphone", "3x", imageName, null, jsonArray);
				break;
			case IPAD:
				addDeviceJsonObject("ipad", "1x", imageName, null, jsonArray);
				break;
			case IPAD_RETINA:
				addDeviceJsonObject("ipad", "2x", imageName, null, jsonArray);
				break;
		}
	}

	public void addIPhoneX1EmptyEntry(JsonArray jsonArray) {
		addDeviceJsonObject("iphone", "1x", null, null, jsonArray);
	}

	private void addDeviceJsonObject(String idiom, String scale, String imageName, String subtype, JsonArray jsonArray) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("idiom", idiom);
		jsonObject.addProperty("scale", scale);
		if(imageName != null) {
			jsonObject.addProperty("filename", imageName);
		}
		if(subtype != null) {
			jsonObject.addProperty("subtype", subtype);
		}

		jsonArray.add(jsonObject);
	}

	public void persistJson(String outputPath, JsonArray devicesArray) {
		logger.log("persisting " + outputPath);
		JsonObject root = new JsonObject();
		root.add("images", devicesArray);

		JsonObject infoElement = new JsonObject();
		infoElement.addProperty("version", 1);
		infoElement.addProperty("author", "xcode");
		root.add("info", infoElement);

		String prettyJsonString = gson.toJson(root);
		try {
			FileWriter file = new FileWriter(outputPath);
			file.write(prettyJsonString);
			file.flush();
			file.close();
		} catch (IOException e) {
			logger.log(ExceptionUtils.getFullStackTrace(e));
		}
	}
}
