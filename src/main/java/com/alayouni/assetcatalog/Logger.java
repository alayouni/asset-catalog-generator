package com.alayouni.assetcatalog;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 */
public interface Logger {

	public void log(String msg);

}
