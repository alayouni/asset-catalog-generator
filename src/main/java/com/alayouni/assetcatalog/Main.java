package com.alayouni.assetcatalog;

import java.io.IOException;
import java.net.URLDecoder;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 */
public class Main {
	public static void main(String[] args) {
		String path = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		Runtime rt = Runtime.getRuntime();
		try {
			String jarPath = URLDecoder.decode(path, "UTF-8");
			System.out.println(jarPath);
			rt.exec(new String[] {"java", "-Xms64m", "-Xmx256m", "-cp", jarPath, "com.alayouni.assetcatalog.MainWindow"});
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
