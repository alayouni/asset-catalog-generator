package com.alayouni.assetcatalog;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 */
public class MainWindow extends JFrame implements Logger{

	private static final String PREFERENCES_JSON_FILE_NAME = "preferences.json";

	private static final String IPAD_RETINA_PREFERENCE_KEY = "ipadRetinaScale";

	private static final String IPHONE_6_PLUS_PREFERENCE_KEY = "iphone6PlusScale";

	private static final String IPHONE_6_PREFERENCE_KEY = "iphone6Scale";

	private static final String IPHONE_5_PREFERENCE_KEY = "iphone5Scale";

	private static final String IPAD_PREFERENCE_KEY = "ipadScale";

	private static final String OUTPUT_PREFIX1_PREFERENCE_KEY = "prefix1";

	private static final String OUTPUT_PREFIX2_PREFERENCE_KEY = "prefix2";

	private static final String OUTPUT1_PARENT_DIRECTORY_KEY = "output1-parent-directory";

	private static final String OUTPUT2_PARENT_DIRECTORY_KEY = "output2-parent-directory";


	private static final double IPHONE_6_PLUS_DEFAULT_SCALE = 1.;

	private static final double IPAD_RETINA_DEFAULT_SCALE = .92753623;

	private static final double IPHONE_6_DEFAULT_SCALE = .60416667;

	private static final double IPHONE_5_DEFAULT_SCALE = .51449275;

	private static final double IPAD_DEFAULT_SCALE = .46376812;

	private static final String OUTPUT_PREFIX1_DEFAULT = "uma-dflt-";

	private static final String OUTPUT_PREFIX2_DEFAULT = "uma-wdn-";

	private static final String OUTPUT1_PARENT_DEFAULT_DIRECTORY = ".";

	private static final String OUTPUT2_PARENT_DEFAULT_DIRECTORY = ".";


	private JTextField iphone6PlusScaleField = null;

	private JTextField ipadRetinaScaleField = null;

	private JTextField iphone6ScaleField = null;

	private JTextField iphone5ScaleField = null;

	private JTextField ipadScaleField = null;

	private JTextArea logArea = null;


	private JTextField outputPrefix1 = null;

	private JTextField outputPrefix2 = null;

	private JTextField output1DirectoryField = null;

	private JTextField output2DirectoryField = null;


	private ImageSetGenerator generator;

	public MainWindow() {
		JPanel contentPane = new JPanel(new BorderLayout(0, 5)) {

			@Override
			public Insets getInsets() {
				return new Insets(20, 20, 20, 20);
			}
		};
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		splitPane.setContinuousLayout(true);
		splitPane.setDividerLocation(300);
		try {
			splitPane.setLeftComponent(getLeftPanel());
		} catch (FileNotFoundException e) {
			this.log(ExceptionUtils.getFullStackTrace(e));
		}
		splitPane.setRightComponent(getLogPanel());
		contentPane.add(splitPane, BorderLayout.CENTER);

		JTextArea infoLabel = new JTextArea("- iPhone 6+ scale applies as is to input image, other scales are relative to iPhone 6+ output dimensions.\n" +
				"- Leave a scale field empty to ignore the corresponding device.\n" +
				"- Leave the second output fields empty to output images into just one target directory.");
		infoLabel.setEditable(false);
		contentPane.add(infoLabel, BorderLayout.SOUTH);

		this.setContentPane(contentPane);


		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize(),
				size = new Dimension(1000, 700);
		this.setSize(size);

		int x = (screenSize.width - size.width) / 2,
				y = (screenSize.height = size.height) / 2;
		this.setLocation(x, y);

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				persistState();
			}
		});

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Asset Catalog Generator");

		generator = new ImageSetGenerator(this);
	}

	private void persistState() {
		JsonObject root = new JsonObject();
		root.addProperty(IPHONE_6_PLUS_PREFERENCE_KEY, readDoubleValue(iphone6PlusScaleField));
		root.addProperty(IPAD_RETINA_PREFERENCE_KEY, readDoubleValue(ipadRetinaScaleField));
		root.addProperty(IPHONE_6_PREFERENCE_KEY, readDoubleValue(iphone6ScaleField));
		root.addProperty(IPHONE_5_PREFERENCE_KEY, readDoubleValue(iphone5ScaleField));
		root.addProperty(IPAD_PREFERENCE_KEY, readDoubleValue(ipadScaleField));
		root.addProperty(OUTPUT_PREFIX1_PREFERENCE_KEY, readTextValue(outputPrefix1, OUTPUT_PREFIX1_DEFAULT));
		root.addProperty(OUTPUT_PREFIX2_PREFERENCE_KEY, readTextValue(outputPrefix2, OUTPUT_PREFIX2_DEFAULT));
		root.addProperty(OUTPUT1_PARENT_DIRECTORY_KEY, readTextValue(output1DirectoryField, OUTPUT1_PARENT_DEFAULT_DIRECTORY));
		root.addProperty(OUTPUT2_PARENT_DIRECTORY_KEY, readTextValue(output2DirectoryField, OUTPUT2_PARENT_DEFAULT_DIRECTORY));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String prettyJsonString = gson.toJson(root);
		try {
			FileWriter file = new FileWriter(PREFERENCES_JSON_FILE_NAME);
			file.write(prettyJsonString);
			file.flush();
			file.close();
		} catch (IOException e) {
			this.log(ExceptionUtils.getFullStackTrace(e));
		}
	}

	private String readTextValue(JTextField textField, String defaultValue) {
		return textField.getText();
	}

	private double readDoubleValue(JTextField textField) {
		if(!textField.getText().isEmpty()) {
			String text = textField.getText();
			try {
				double value = Double.parseDouble(text);
				return value;
			} catch(NumberFormatException e) {
				logArea.append(text + ": wrong format!");
			}
		}
		return 0;
	}

	private JPanel getLeftPanel() throws FileNotFoundException {
		File file = new File(PREFERENCES_JSON_FILE_NAME);
		Map jsonRoot = null;
		if(file.exists()) {
			BufferedReader br = new BufferedReader(new FileReader(file));
			jsonRoot = new Gson().fromJson(br, Map.class);
		}

		JPanel leftPanel = new JPanel(new GridLayout(11, 1, 10, 10));

		iphone6PlusScaleField = createScaleTextField(IPHONE_6_PLUS_PREFERENCE_KEY, IPHONE_6_PLUS_DEFAULT_SCALE, jsonRoot);
		JPanel iphone6PlusPanel = wrapTextField("*IPhone 6+ Scale:", iphone6PlusScaleField);
		leftPanel.add(iphone6PlusPanel);

		ipadRetinaScaleField = createScaleTextField(IPAD_RETINA_PREFERENCE_KEY, IPAD_RETINA_DEFAULT_SCALE, jsonRoot);
		JPanel ipadRetinaPanel = wrapTextField("IPad Retina Scale:", ipadRetinaScaleField);
		leftPanel.add(ipadRetinaPanel);

		iphone6ScaleField = createScaleTextField(IPHONE_6_PREFERENCE_KEY, IPHONE_6_DEFAULT_SCALE, jsonRoot);
		JPanel iphone6Panel = wrapTextField("IPhone 6 Scale:", iphone6ScaleField);
		leftPanel.add(iphone6Panel);

		iphone5ScaleField = createScaleTextField(IPHONE_5_PREFERENCE_KEY, IPHONE_5_DEFAULT_SCALE, jsonRoot);
		JPanel iphone5Panel = wrapTextField("IPhone 5 Scale:", iphone5ScaleField);
		leftPanel.add(iphone5Panel);

		ipadScaleField = createScaleTextField(IPAD_PREFERENCE_KEY, IPAD_DEFAULT_SCALE, jsonRoot);
		JPanel ipadPanel = wrapTextField("IPad Scale:", ipadScaleField);
		leftPanel.add(ipadPanel);

		final JButton generateButton = createGenerateButton();
		leftPanel.add(generateButton);

		outputPrefix1 = createTextField(OUTPUT_PREFIX1_PREFERENCE_KEY, OUTPUT_PREFIX1_DEFAULT, jsonRoot);
		JPanel prefix1Panel = wrapTextField("Output prefix 1:", outputPrefix1);
		leftPanel.add(prefix1Panel);

		outputPrefix2 = createTextField(OUTPUT_PREFIX2_PREFERENCE_KEY, OUTPUT_PREFIX2_DEFAULT, jsonRoot);
		JPanel prefix2Panel = wrapTextField("Output prefix 2:", outputPrefix2);
		leftPanel.add(prefix2Panel);

		output1DirectoryField = createTextField(OUTPUT1_PARENT_DIRECTORY_KEY, OUTPUT1_PARENT_DEFAULT_DIRECTORY, jsonRoot);
		JPanel directory1Panel = wrapTextField("Output 1 Directory", output1DirectoryField);
		leftPanel.add(directory1Panel);

		output2DirectoryField = createTextField(OUTPUT2_PARENT_DIRECTORY_KEY, OUTPUT2_PARENT_DEFAULT_DIRECTORY, jsonRoot);
		JPanel directory2Panel = wrapTextField("Output 2 Directory", output2DirectoryField);
		leftPanel.add(directory2Panel);

		final JButton resetButton= createResetButton();
		leftPanel.add(resetButton);

		return leftPanel;
	}

	private JButton createGenerateButton() {
		final JButton generateButton = new JButton("Generate Image Set(s)");
		generateButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				generateButton.setEnabled(false);
				new Thread(new Runnable() {
					@Override
					public void run() {
						OutputDescriptor[] prefixes = readOutputDescriptors();
						double iPhone6PlusScale = readDoubleValue(iphone6PlusScaleField);
						generator.generateImageSets(iPhone6PlusScale,
								iPhone6PlusScale * readDoubleValue(ipadRetinaScaleField),
								iPhone6PlusScale * readDoubleValue(iphone6ScaleField),
								iPhone6PlusScale * readDoubleValue(iphone5ScaleField),
								iPhone6PlusScale * readDoubleValue(ipadScaleField),
								prefixes);

						generateButton.setEnabled(true);
					}
				}).start();
			}
		});
		return generateButton;
	}

	private JButton createResetButton() {
		JButton resetButton = new JButton("Reset Scale Defaults");
		resetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				iphone6PlusScaleField.setText("" + IPHONE_6_PLUS_DEFAULT_SCALE);
				ipadRetinaScaleField.setText("" + IPAD_RETINA_DEFAULT_SCALE);
				iphone6ScaleField.setText("" + IPHONE_6_DEFAULT_SCALE);
				iphone5ScaleField.setText("" + IPHONE_5_DEFAULT_SCALE);
				ipadScaleField.setText("" + IPAD_DEFAULT_SCALE);
			}
		});
		return resetButton;
	}

	private OutputDescriptor[] readOutputDescriptors() {
		OutputDescriptor descriptor1 = new OutputDescriptor();
		descriptor1.setPrefix(outputPrefix1.getText());
		descriptor1.setParentDirectory(readTextValue(output1DirectoryField, OUTPUT1_PARENT_DEFAULT_DIRECTORY));
		if(outputPrefix2.getText().isEmpty()) {
			return new OutputDescriptor[] {descriptor1};
		} else {
			OutputDescriptor descriptor2 = new OutputDescriptor();
			descriptor2.setPrefix(outputPrefix2.getText());
			descriptor2.setParentDirectory(readTextValue(output2DirectoryField, OUTPUT2_PARENT_DEFAULT_DIRECTORY));
			return new OutputDescriptor[] {descriptor1, descriptor2};
		}
	}

	private JScrollPane getLogPanel() {
		logArea = new JTextArea();
		logArea.setEditable(false);
		return new JScrollPane(logArea);
	}

	private JTextField createScaleTextField(String preferenceKey, double defaultValue, Map jsonRoot) {
		Double value;
		if(jsonRoot == null) {
			value = defaultValue;
		} else {
			value = (Double)jsonRoot.get(preferenceKey);
			if(value == null) {
				value = defaultValue;
			}
		}
		JTextField textField = new JTextField(((value == 0) ? "" : ("" + value)));
		return textField;
	}

	private JTextField createTextField(String preferenceKey, String defaultValue, Map jsonRoot) {
		String value = "";
		if(jsonRoot == null) {
			value = defaultValue;
		} else {
			value = (String)jsonRoot.get(preferenceKey);
			if(value == null) {
				value = defaultValue;
			}
		}
		JTextField textField = new JTextField(value);
		return textField;
	}

	private JPanel wrapTextField(String labelText, JTextField textField) {
		JPanel panel = new JPanel(new BorderLayout(10, 0));

		JLabel label = new JLabel(labelText);
		label.setPreferredSize(new Dimension(120, 30));
		panel.add(label, BorderLayout.WEST);

		panel.add(textField, BorderLayout.CENTER);

		return panel;
	}

	public static void main(String[] args) {
		new MainWindow().setVisible(true);
	}

	@Override
	public void log(final String msg) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				logArea.append(msg + "\n");
			}
		});
	}
}
