package com.alayouni.assetcatalog;

import com.google.gson.JsonArray;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 */
public class OutputDescriptor {

	private String parentDirectory;

	private String prefix;

	private JsonArray jsonArray = new JsonArray();

	public void resetJsonArray() {
		jsonArray = new JsonArray();
	}

	public String jsonOutputPath(String inputName) {
		return parentDirectory + "/" + prefix + inputName + ".imageset/Contents.json";
	}

	public String getParentDirectory() {
		return parentDirectory;
	}

	public void setParentDirectory(String parentDirectory) {
		this.parentDirectory = parentDirectory;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public JsonArray getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(JsonArray jsonArray) {
		this.jsonArray = jsonArray;
	}
}
